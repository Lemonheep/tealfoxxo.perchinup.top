/*
Header and Footer
*/
// Select Header
const headerText = document.querySelector("header");
// Select Footer
const footerText = document.querySelector("footer")

// Header items
headerText.innerHTML = 
`<div>
<ul class="menu">
    <li><strong><a href="../index.html">Poppy Farbird's Web-Space!</a></strong></li>
    <li><a href="../pages-menu/digital_art.html">Digital Art!</a></li>
    <li><a href="../pages-menu/irl_art.html">IRL Art!</a></li>
    <li><a href="../pages-menu/cw.html">NSFW!</a></li>
    <li><a href="../pages-menu/network_suite.html">Network Suite!</a></li>
    <li><a href="../pages-menu/writings.html">Writings!</a></li>
    <li><a href="../pages-menu/others_cool_stuff.html">Others' Cool Stuff!</a></li>
    <li><a href="../pages-menu/socials.html">Socials!</a></li>
    <li><a href="../pages-menu/about.html">About Me!</a></li>
</ul>
</div>`
;

footerText.innerHTML = 
`
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><span property="dct:title">This website</span> by <span property="cc:attributionName">Poppy Farbird</span> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>`
;